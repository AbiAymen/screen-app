import React, { useState } from 'react';
import { makeStyles} from '@material-ui/core/styles';

import { TextField, Button, Paper, Typography, fade} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import AuthenticationService from '../../Services/AuthenticationService';
import FileCopySharpIcon from '@material-ui/icons/FileCopySharp';
import { blue } from '@material-ui/core/colors';
import { motion } from "framer-motion"

const useStyles = makeStyles((theme) => ({
  root: {
    
    margin : 'auto',
    width : '25%',
    marginTop : "5%",
    padding : '2.5%',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.75),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.60),
   }

  },
 container : {
   padding : '2.5%',
   margin : 'auto'
 },
 separator : {
   height : "15px"
 },
 }));

 const variants = {
  visible: { opacity: 1 },
  hidden: { opacity: 0 },
}
export default function Login() {
  const classes = useStyles();
  const history = useHistory();
  const [username , SetUsername] = useState('');
  const [password , SetPassword] = useState('');
  
  return (
    <div style={{background:`linear-gradient(${blue[500]}, #100713)`, height : "100%",position :"absolute" , width:"100%"}}
   
    >
      <motion.div
       initial="hidden"
       animate="visible"
       variants={variants}
       >
   <Paper elevation={3} className={classes.root}>
     <div style={{}}>
           <FileCopySharpIcon color="primary" fontSize="inherit" style={{width : "30%" , height : "30%",paddingLeft : "35%"}}/>
           <Typography color="primary" style={{paddingLeft : "16%"}} variant='h3'>CB IntInt 1.0.0</Typography></div>
           <div style={{height:"25px"}}></div>
           
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="Username"
              variant="outlined"
              onChange={(e)=>{SetUsername(e.target.value)}}
            />
            <div className={classes.separator}></div>
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="Password"
              type="password"
              variant="outlined"
              onChange={(e)=>{SetPassword(e.target.value)}}
            />
          <div className={classes.separator}></div>
            <Button
              style={{ width: "100%" }}
              variant="contained"
              color="primary"
              size="large"
              onClick={()=> {history.push("/dashboard");
              AuthenticationService(username , password).then((res)=>{
                console.log(res)
              })
            }}
            >
              Connexion
            </Button>
        
     
      </Paper>
      </motion.div>
      </div>
  );
}
