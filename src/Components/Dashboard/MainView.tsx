import React, { useEffect, useState } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from "@material-ui/core/Paper";
import { TablePagination, Button, Typography, LinearProgress, Collapse, IconButton, Checkbox, Tabs, AppBar, Tab, useTheme} from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import FilesListService from '../../Services/FilesListService';
import DirectoryNamesService from '../../Services/DirectoryNamesService';
import SendingFilesListSerivce from '../../Services/SendingFilesListService';
import SwipeableViews from 'react-swipeable-views';
import Account from './ClientAccount';
import AccountService from '../../Services/AccountsService'
import StyledTableRow from '../CommonUse/StyledTable';
import {StyledTableCell} from '../CommonUse/StyledTable'
import TabPanel from '../CommonUse/Tabs';
import {a11yProps} from '../CommonUse/Tabs';
import PathsCard from './PathsCard'



function createData(
  filename: string,
  Date: string,
  entity: string,
  Inputfileblob: string,
  
) {
  return {
    filename,
    Date,
    entity,
    Inputfileblob
  };
}


   


function Row(props: { row: ReturnType<typeof createData>}) {
  const [open, SetOpen] = useState(false)


  const { row } = props;
  return (
    <React.Fragment>
      <StyledTableRow >
        <StyledTableCell component="th" scope="row" align="left">
          {row.filename}
        </StyledTableCell>
        <StyledTableCell align="center">{row.Date}</StyledTableCell>
       
        <StyledTableCell align="center">{row.entity}</StyledTableCell>
        <StyledTableCell>
        <IconButton aria-label="expand row" size="small" onClick={() => SetOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </StyledTableCell>
        <StyledTableCell>
        <IconButton aria-label="expand row" size="small" onClick={() => SetOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </StyledTableCell>
        
      </StyledTableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={8}>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <div style={{maxHeight :"250px" , overflowY :'scroll'}}>
          <Typography>{row.Inputfileblob}</Typography>
           </div>
        
        </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}
export default function MainView(props : {FilesService : any , InputPath : string , OutputPath : string , ArchivePath : string , SetInputPath : any , SetOutputPath : any , SetArchivePath :any}) {
  const [table, setTable] = useState([
    createData("", "", "", ""),
  ]);
  const [tableUpdated, SetTableUpdated] = useState(false)
  const [page , SetPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [start,SetStart] = useState(false)
  const [loading , SetLoading] = useState(true)
  const [filesTable, SetFilesTable] = useState([{path : ''}]);

  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };
  

  

  var indexTable :number[]= [];
  
 function onStart(){
   var midtable = filesTable;
  indexTable.forEach((i)=>{
    midtable.splice(i,1)
  })
  SetFilesTable(midtable)
  console.log(filesTable)
  SendingFilesListSerivce(filesTable)
  .then((res) => {
    console.log(res)
    return res.json()})
  .then((res: { applicationSource: string; dateTrait: string; fileName: string; fileblob : string}[]) => {
    console.log(res)
    
    var midTable: {
      filename: string;
      Date: string;
      entity: string;
      Inputfileblob : string;
      
      
    }[] = [];
    SetLoading(false)
    res.forEach(
      (element: {
        applicationSource: string;
        dateTrait: string;
        fileName: string;
        fileblob: string;
       
      }) => {
        console.log(element)
        midTable.push(
          createData(
            element.fileName,
            element.dateTrait,
            element.applicationSource,
            atob(element.fileblob),
           
          )
        );
      }
    );
    setTable(midTable);
    console.log(table)
    SetTableUpdated(!tableUpdated);
  });
 }


useEffect(() => {  
  DirectoryNamesService().then((res)=>{
     console.log(res)
    res.json().then((res)=>{
      console.log(res.content)
      props.SetInputPath(res.content[0].repIn)
      props.SetOutputPath(res.content[0].repOut)
      props.SetArchivePath(res.content[0].repArchiv)
    })
  })
  
  
  
  
  
  FilesListService()
  .then((res)=>res.json())
  .then((res)=>{
    console.log(res)
    var midtable : {path : string}[]=[];
    res.forEach((element: { path: string; })=>{
      midtable.push(element)
    })
    console.log(midtable)
    SetFilesTable(midtable)
    
  })
  }, [page,rowsPerPage,start])
  return (
    <div>
      
     <PathsCard InputPath={props.InputPath} OutputPath={props.OutputPath} ArchivePath={props.ArchivePath} start={start} setStart={SetStart} loading={loading} filesTableLength={filesTable.length} onStart={onStart}/>
       
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Fichiers" {...a11yProps(0)} />
          <Tab label="Comptes" {...a11yProps(1)} />
         
          
        </Tabs>
      </AppBar>
        <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
        {start ? (
        <div style={{ margin: "auto" }}>
          {loading ? (
            <div>
              <LinearProgress />
            </div>
          ) : (
            <div>
              <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                  <TableHead>
                    <StyledTableRow>
                      <StyledTableCell align="left">
                        Fichier
                      </StyledTableCell>
                      <StyledTableCell align="center">Date</StyledTableCell>
                     
                      <StyledTableCell align="center">
                        Entité
                      </StyledTableCell>
                    
                      <StyledTableCell>
                      Input
                      </StyledTableCell>
                      <StyledTableCell>
                      Output
                      </StyledTableCell>
                    </StyledTableRow>
                  </TableHead>
                  <TableBody>
                    {table.map((row) => (
                      <Row key={row.filename} row={row}/>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 20]}
                component="div"
                count={1000}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={(event, newPage) => {
                  SetPage(newPage);
                }}
                onChangeRowsPerPage={(event) => {
                  setRowsPerPage(+event.target.value);
                  SetPage(0);
                }}
              />
            </div>
          )}
        </div>
      ) : (
        <div>
          <Table>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell>Fichier</StyledTableCell>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
                
              </StyledTableRow>
            </TableHead>
            <TableBody>
             {filesTable.map((element)=>(
               <StyledTableRow>
                 <StyledTableCell> <Checkbox
                        defaultChecked
                          onChange={(e)=>{if(!e.target.checked){
                        if(!indexTable.includes(filesTable.indexOf(element)))
                        {indexTable.push(filesTable.indexOf(element))}}
                        else if (e.target.checked){
                          if(indexTable.includes(filesTable.indexOf(element))){
                            indexTable.splice(indexTable.indexOf(filesTable.indexOf(element)),1)
                          }
                        }
                        console.log(indexTable)}}
                        /></StyledTableCell>
             <StyledTableCell>{element.path}</StyledTableCell>
                 <StyledTableCell></StyledTableCell>
                 <StyledTableCell></StyledTableCell>
                 <StyledTableCell></StyledTableCell>
                 <StyledTableCell></StyledTableCell>
                
               </StyledTableRow>
             ))}
            </TableBody>
          </Table>
        </div>
      )}
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
         <Account AccountsService={AccountService}/>
        </TabPanel>
        </SwipeableViews>
     
    </div>
  );
}