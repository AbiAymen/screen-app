import React from 'react';
import { fade, makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import { Avatar, withStyles, ThemeProvider, useTheme} from '@material-ui/core';
import StorageIcon from '@material-ui/icons/Storage';
import {EndPoint} from '../../Services/ServicesConfig'
import FileCopySharpIcon from '@material-ui/icons/FileCopySharp';
import ComputerSharpIcon from '@material-ui/icons/ComputerSharp';
import { blue } from '@material-ui/core/colors';

const StyledBadge = withStyles((theme: Theme) =>
  createStyles({
    badge: {
      backgroundColor: '#44b700',
      color: '#44b700',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
  }),
)(Badge);


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
      
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  }),
);


export default function AppToolBar(props : {url : string , port : number}) {
  const classes = useStyles();
 
 const theme  = useTheme()
  

 
 
  
  return (
    <div className={classes.grow}>
      <AppBar position="static" color='inherit' style={{ background: `linear-gradient(${blue[800]}, #100713)` , color :'white' }}>
        <Toolbar>
          
          <FileCopySharpIcon style={{marginRight : '0.75%'}}/>
         
          <Typography className={classes.title} variant="h5" noWrap>
           CB IntInt 1.0.0
          </Typography>
          <Toolbar />
          <StyledBadge overlap="circle"
          anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        variant="dot" style={{marginRight : '1%'}}>
           <ComputerSharpIcon style={{marginRight : '0.5%'}}/>
           </StyledBadge>
          
           <Typography variant="body2">{EndPoint}</Typography>
           <Toolbar />
          {props.url!=='' ? <StyledBadge overlap="circle"
          anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        variant="dot" style={{marginRight : '1%'}}>
           <StorageIcon />
           </StyledBadge> : <StorageIcon />}
           
           <div style={{display : "flex", justifyContent : "flex-start"}}>
           <Typography style={{marginRight : '2%'}} variant="body2">{props.url}</Typography>
           <Typography style={{marginRight : '2%'}} variant="body2">:</Typography>
           <Typography variant="body2">{props.port}</Typography>
           </div>
           
         
          <div className={classes.grow} />
          <Avatar component='button' alt="Remy Sharp" src="https://mdbootstrap.com/img/Photos/Slides/img%20(115).jpg" onClick={()=>{console.log('click')}}/>
       
         
        </Toolbar>
   
      </AppBar>
      
    </div>
  );
}