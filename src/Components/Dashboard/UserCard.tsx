import React from 'react';
import { Paper, Avatar, Typography } from '@material-ui/core';

export default function UserCard(){
    return (
        <div>
            <Paper elevation={3} style={{margin : "2.5%",padding:"5%"}} >
            <Avatar style={{margin : "auto", marginBottom : "10%",height : "100px", width : "100px"}} alt="Remy Sharp" src="https://mdbootstrap.com/img/Photos/Slides/img%20(115).jpg" />
            <Typography variant="body1">Utilisateur : Test user</Typography>
            <Typography variant="body1">Role : Administrateur</Typography>
            </Paper>
        </div>
    )
}