import React, { useState, useEffect } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import ToolBar from './AppBar';
import MainView from './MainView';
import { Tabs, Tab, Box, Divider } from "@material-ui/core";
import TocIcon from '@material-ui/icons/Toc';

import SettingsIcon from '@material-ui/icons/Settings';

import Settings from '../Settings/Settings';
import FilesService from '../../Services/FilesService';

import { motion } from "framer-motion";
import UserCard from './UserCard';
import getDatabasConfigService from '../../Services/DatabaseAccessService';



const drawerWidth = 240;
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      backgroundColor : theme.palette.primary.light
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      background: "linear-gradient(to right bottom,theme.palette.primary.light,#ffffff00)",
      color: "red"
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      backgrounColor : theme.palette.primary.light
    },
    drawerPaper: {
      width: drawerWidth,
      backgrounColor : theme.palette.primary.light
    },
   content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    tabs: {
      borderRight: `1px solid ${theme.palette.divider}`,
    },
  }),
);


function TabPanel(props: TabPanelProps) {
  const { children, value, index } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}


export default function GlobalView() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  
  const [InputPath , SetInputPath] = useState('');
  const [OutputPath , SetOutputPath] = useState('');
  const [ArchivePath , SetArchivePath] = useState('');

  const[dbconfigId , SetdbconfigId] = useState(0);
  const[ url , seturl] = useState('')
  const[ port , setport] = useState(0)
  const[ sid , setsid] = useState('')
  const[ user , setuser] = useState('')
  const[ pswd , setpswd] = useState('')

  useEffect(()=>{
    
    getDatabasConfigService().then((res )=>{
      res.json().then((res)=>{
          if(res.length!==0){
              console.log(res)
              seturl(res[0].url)
              setport(res[0].port)
              setsid(res[0].sid)
              setuser(res[0].user)
              setpswd(res[0].pswd)
              SetdbconfigId(res[0].id)
          }
      })
    
   })
  })

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <ToolBar
          url = {url}
          port = {port}
          
        />
      </AppBar>

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <UserCard />
        <Toolbar />
        <Divider variant="middle" />
        <motion.div
        >
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs example"
            className={classes.tabs}
          >
            <Tab
              label={
                <div
               
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <TocIcon style={{ marginRight: "20px" }} />
                  <Typography color="textPrimary" variant="body2">
                    Tableau de bord
                  </Typography>
                </div>
              }
              {...a11yProps(0)}
            />
            <Divider variant="middle" />
            <Tab
              label={
                <div
               
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "center",
                    width: "100%",
                  }}
                >
                  <SettingsIcon style={{ marginRight: "20px" }} />
                  <Typography color="textPrimary" variant="body2">
                  Paramêtres
                  </Typography>
                </div>
              }
              {...a11yProps(1)}
            />
            <Divider variant="middle" />
           
            
            
          </Tabs>
        </motion.div>
      </Drawer>

      <main className={classes.content}>
        <Toolbar />
        <TabPanel value={value} index={0}>
          <MainView
            FilesService={FilesService}
           
            InputPath={InputPath}
            OutputPath={OutputPath}
            ArchivePath={ArchivePath}
            SetInputPath={SetInputPath}
            SetOutputPath={SetOutputPath}
            SetArchivePath={SetArchivePath}
          />
        </TabPanel>
        <TabPanel value={value} index={1}></TabPanel>
        <TabPanel value={value} index={2}>
        <Settings
            
            InputPath={InputPath}
            OutputPath={OutputPath}
            ArchivePath={ArchivePath}
            SetInputPath={SetInputPath}
            SetOutputPath={SetOutputPath}
            SetArchivePath={SetArchivePath}
            dbConfigId={dbconfigId}
            url = {url}
            port = {port}
            sid = {sid}
            username = {user}
            password = {pswd}
          />
        </TabPanel>
        <TabPanel value={value} index={3}>
       
          
        </TabPanel>
        <TabPanel value={value} index={4}>
         
        </TabPanel>
        <TabPanel value={value} index={5}></TabPanel>
       
      </main>
    </div>
  );
}