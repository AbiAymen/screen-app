import React from 'react' ;
import { Paper, Table, TableHead, Button, TableBody, Typography } from '@material-ui/core';
import {StyledTableCell} from '../CommonUse/StyledTable';
import StyledTableRow from '../CommonUse/StyledTable';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import HourglassFullRoundedIcon from '@material-ui/icons/HourglassFullRounded';



export default function PathsCard(props : {InputPath : string , OutputPath : string , ArchivePath : string , filesTableLength : number , start : boolean  , setStart : any , loading : boolean , onStart  : any}) {
    return (
    
  
   <Paper elevation={3} style={{ marginBottom: "10px", padding: "15px"}}>
        
          <Paper style={{marginBottom : '2%'}}>
          <Table>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell>Répertoire</StyledTableCell>
                <StyledTableCell>Chemin</StyledTableCell>
                <StyledTableCell>Nombre de fichiers</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              <StyledTableRow>
                <StyledTableCell>Input</StyledTableCell>
        <StyledTableCell>{props.InputPath}</StyledTableCell>
        <StyledTableCell>{props.filesTableLength}</StyledTableCell>
              </StyledTableRow>
              <StyledTableRow>
              <StyledTableCell>Output</StyledTableCell>
        <StyledTableCell>{props.OutputPath}</StyledTableCell>
        <StyledTableCell>N/A</StyledTableCell>
              </StyledTableRow>
              <StyledTableRow>
              <StyledTableCell>Archive</StyledTableCell>
        <StyledTableCell>{props.ArchivePath}</StyledTableCell>
        <StyledTableCell>N/A</StyledTableCell>
              </StyledTableRow>
            </TableBody>
          </Table>
          </Paper>
          <div style={{width : '100%' , paddingLeft : '45%'}}>
          <Button
            color="primary"
            variant="contained"
            size="medium"
            style={{width : '10%' ,marginRight : '50px'}}
            onClick={() => {
              props.setStart(!props.start);
              props.onStart()
            }}
          >
            Start
          </Button>
          </div>
        
        {props.start ? (
          <div>
            {props.loading ? (
              <div style={{ display: "flex", justifyContent: "flex-start" }}>
                <HourglassFullRoundedIcon
                  color="inherit"
                  style={{ color: "orange", paddingRight: "5px" }}
                />
                <Typography color="inherit" style={{ color: "orange" }}>
                  Chargement...
                </Typography>
              </div>
            ) : (
              <div style={{ display: "flex", justifyContent: "flex-start" }}>
                <CheckCircleRoundedIcon
                  color="inherit"
                  style={{ color: "green", paddingRight: "5px" }}
                />
                <Typography color="inherit" style={{ color: "green" }}>
                  En cours
                </Typography>
              </div>
            )}
          </div>
        ) : (
          <div></div>
        )}
      </Paper>
   
      
    )
}