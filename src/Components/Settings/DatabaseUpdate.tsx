import React, { useState} from 'react';
import {TextField, Button, Paper, Snackbar} from '@material-ui/core';
import {postDatabaseConfigService,putDatabaseConfigService, testDatabaseConfigService} from '../../Services/DatabaseAccessService';
import Alert from '@material-ui/lab/Alert';

import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import ReactNotification from 'react-notifications-component'


export default function DatabaseUpdate(props :{id : number , url : string , port : number , sid : string , user : string , pswd : string}){
     
    const[url,seturl] = useState(props.url)
    const[port,setport] = useState(props.port)
    const[sid,setsid] = useState(props.sid)
    const[user,setuser] = useState(props.user)
    const[pswd,setpswd] = useState(props.pswd)

    const [SuccesOpen, setSuccesOpen] = useState(false);
    const [ErrorOpen, setErrorOpen] = useState(false);
   
    const handleSuccessClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
        setSuccesOpen(false);
      };

      const handleErrorClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
        setErrorOpen(false);
      };


    return (
        <div>
         <ReactNotification />
        <Snackbar open={SuccesOpen} autoHideDuration={30000} onClose={handleSuccessClose}>
          <Alert onClose={handleSuccessClose} severity="success">
            Configuration enregistrée avec succés
          </Alert>
        </Snackbar>
        <Snackbar open={SuccesOpen} autoHideDuration={30000} onClose={handleSuccessClose}>
          <Alert onClose={handleSuccessClose} severity="error">
            Configuration erronée
          </Alert>
        </Snackbar>
            <Paper elevation={2} style={{padding :"2.5%" , width :"60%",margin : "auto"}}>
           
            <div style={{display:'flex',justifyContent:'space-around'}}>
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="Domaine"
              variant="outlined"
              defaultValue={url}
              onChange={(e)=>{seturl(e.target.value)}}
            />
            
            <TextField
              style={{ width: "100%" }}
              id="standard-password-input"
              label="Port"
              variant="outlined"
              defaultValue={port}
              onChange={(e)=>{setport(parseInt(e.target.value))}}
            />
             </div>
            <div style={{height: "15px"}}></div>
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="SID"
              variant="outlined"
              defaultValue={sid}
              onChange={(e)=>{setsid(e.target.value)}}
            />
            <div style={{height: "15px"}}></div>
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="Username"
              variant="outlined"
              defaultValue={user}
              onChange={(e)=>{setuser(e.target.value)}}
            />
            <div style={{height: "15px"}}></div>
            <TextField
              style={{ width: "100%" }}
              id="outlined-basic"
              label="Password"
              type="password"
              variant="outlined"
              defaultValue={pswd}
              onChange={(e)=>{setpswd(e.target.value)}}
            />
             <div style={{height: "15px"}}></div>
            <div style={{width:'100%', paddingLeft : '45%'}}>
            <Button
            color="primary"
            variant="contained"
            size="medium"
            onClick={() => {
                
            testDatabaseConfigService(url , port , sid , user , pswd).then((res)=>{
             if(res){
                store.addNotification({
                    title: "Connexion établie",
                    message: `La connnexion a la base de données ${url} : ${port} a été établie avec succées`,
                    type: "success",
                    insert: "bottom",
                    container: "bottom-right",
                    animationIn: ["animate__animated", "animate__fadeIn"],
                    animationOut: ["animate__animated", "animate__fadeOut"],
                    dismiss: {
                      duration: 2000,
                      onScreen: true
                    }
                  });
                if(props.id===0){
                    postDatabaseConfigService(url , port , sid , user , pswd).then((res)=>{
                        if(res.status===200){
                            store.addNotification({
                                title: "Configuration mise à jour",
                                message: `La configuration a été mise à jour avec succés`,
                                type: "success",
                                insert: "bottom",
                                container: "bottom-right",
                                animationIn: ["animate__animated", "animate__fadeIn"],
                                animationOut: ["animate__animated", "animate__fadeOut"],
                                dismiss: {
                                  duration: 2000,
                                  onScreen: true
                                }
                              });
                        }
                    })
                    }
                else{
                    putDatabaseConfigService(props.id , url , port , sid , user , pswd).then((res)=>{
                        if(res.status===200){
                            store.addNotification({
                                title: "Nouvelle configuration ajoutée",
                                message: `La nouvelle configuration a été ajoutée avec succés`,
                                type: "success",
                                insert: "bottom",
                                container: "bottom-right",
                                animationIn: ["animate__animated", "animate__fadeIn"],
                                animationOut: ["animate__animated", "animate__fadeOut"],
                                dismiss: {
                                  duration: 2000,
                                  onScreen: true
                                }
                              });
                        }
                    })
                }
             }
             else{
                store.addNotification({
                    title: "Connexion non établie",
                    message: `La configuration entrée est erronée`,
                    type: "danger",
                    insert: "bottom",
                    container: "bottom-right",
                    animationIn: ["animate__animated", "animate__fadeIn"],
                    animationOut: ["animate__animated", "animate__fadeOut"],
                    dismiss: {
                      duration: 2000,
                      onScreen: true
                    }
                  });
             }
            })    
            }}
          >
            Sauvegarder
          </Button>
          </div>
          </Paper>
        </div>
    )
}