import React, { useState, useEffect } from 'react';
import {TextField, Button, Paper, Toolbar, Snackbar, Table, TableHead, TableBody, AppBar, Tabs, Tab, useTheme } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import RepertoireService from '../../Services/DirectorySettingsService';
import CircularProgress from '@material-ui/core/CircularProgress';
import DirectoryUpdateSerivce from '../../Services/DirectoryUpdateService';
import SwipeableViews from 'react-swipeable-views';
import MatricesUpdate from './MatricesUpdate';
import DatabaseUpdate from './DatabaseUpdate';
import {StyledTableCell} from '../CommonUse/StyledTable';
import StyledTableRow from '../CommonUse/StyledTable';
import TabPanel from '../CommonUse/Tabs'
import {a11yProps} from '../CommonUse/Tabs'








export default function Settings(props : {InputPath : string , OutputPath : string , ArchivePath : string , SetInputPath : any , SetOutputPath : any , SetArchivePath : any ,dbConfigId : number , url : string , port : number , sid : string , username : string , password : string}) {

  const [open, setOpen] = useState(false);
  const [InputPath , SetInputPath] = useState(props.InputPath);
  const [OutputPath , SetOutputPath] = useState(props.OutputPath);
  const [ArchivePath , SetArchivePath] = useState(props.ArchivePath);
  const [InputExist, SetInputExist] = useState(false);
  const [OutputExist, SetOutputExist] = useState(false);
  const [ArchiveExist, SetArchiveExist] = useState(false);
  const [InputLoading , SetInputLoading] = useState(true);
  const [OutputLoading , SetOutputLoading] = useState(true);
  const [ArchiveLoading , SetArchiveLoading] = useState(true);
  
 

  const[InputError, setInputError] = useState(false)
  const[OutputError, setOutputError] = useState(false)
  const[ArchiveError, setArchiveError] = useState(false)




  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  
  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };



  useEffect(()=>{
    
    RepertoireService(InputPath).then((res)=>{
     SetInputExist(res)
     SetInputLoading(false)
    })
    RepertoireService(OutputPath).then((res)=>{
      SetOutputExist(res)
      SetOutputLoading(false)
     })
     RepertoireService(ArchivePath).then((res)=>{
      SetArchiveExist(res)
      SetArchiveLoading(false)
     })

  })

  return (
    <div>
      
      <Paper elevation={3} style={{ marginBottom: "10px", padding: "15px" }}>
        <Snackbar open={open} autoHideDuration={30000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="success">
            Settings updated succesfully !
          </Alert>
        </Snackbar>
      
        
        <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Répertoires" {...a11yProps(0)} />
          <Tab label="Matrices" {...a11yProps(1)} />
          <Tab label="Base de données" {...a11yProps(1)} />
          
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
        <Paper elevation={2}>
        <Table>
          <TableHead>
            <StyledTableRow>
              <StyledTableCell>Répertoire</StyledTableCell>
              <StyledTableCell>Chemin</StyledTableCell>
              <StyledTableCell>Existence</StyledTableCell>
            </StyledTableRow>
          </TableHead>
          <TableBody>
            <StyledTableRow>
              <StyledTableCell>Input</StyledTableCell>
               <StyledTableCell>
                <TextField
                  defaultValue={InputPath}
                  id="outlined-basic"
                  label="Input"
                  variant="outlined"
                  helperText={InputError ? 'Le chemin ne doit pas contenir que des \\ seulement des /' : ''}
                  size="small"
                  error={InputError}
                  onChange={(e) => {
                    if(e.target.value.includes('\\')){
                      setInputError(true)
                      }
                      else {
                        setInputError(false)
                        
                      }
                    SetInputPath(e.target.value);
                    SetInputLoading(true);
                    console.log(InputPath);
                    RepertoireService(InputPath).then((res) => {
                      SetInputExist(res);
      
                      SetInputLoading(false);
                    });
                  }}
                />
              </StyledTableCell>
              <StyledTableCell>
                {InputLoading ? (
                  <CircularProgress size={30} />
                ) : (
                  <div>
                    {" "}
                    {InputExist ? (
                      <CheckCircleIcon
                        color="inherit"
                        style={{ color: "green" }}
                      />
                    ) : (
                      <ErrorIcon color="inherit" style={{ color: "red" }} />
                    )}
                  </div>
                )}
              </StyledTableCell>
            </StyledTableRow>
            <StyledTableRow>
              <StyledTableCell>Output</StyledTableCell>
              <StyledTableCell>
                <TextField
                  defaultValue={OutputPath}
                  id="outlined-basic"
                  label="Output"
                  variant="outlined"
                  helperText={OutputError ? 'Le chemin ne doit pas contenir que des \\ seulement des /' : ''}
                  size="small"
                  error={OutputError}
                  onChange={(e) => {
                    if(e.target.value.includes('\\')){
                      setOutputError(true)
                      }
                      else {
                        setOutputError(false)
                        
                      }
                    SetOutputPath(e.target.value);
                    SetOutputLoading(true);
                    RepertoireService(OutputPath).then((res) => {
                      SetOutputExist(res);
                      SetOutputLoading(false);
                    });
                  }}
                />
              </StyledTableCell>
              <StyledTableCell>
                {OutputLoading ? (
                  <CircularProgress size={30} />
                ) : (
                  <div>
                    {OutputExist ? (
                      <CheckCircleIcon
                        color="inherit"
                        style={{ color: "green" }}
                      />
                    ) : (
                      <ErrorIcon color="inherit" style={{ color: "red" }} />
                    )}
                  </div>
                )}
              </StyledTableCell>
            </StyledTableRow>
            <StyledTableRow>
              <StyledTableCell>Archive</StyledTableCell>
              <StyledTableCell>
                <TextField
                  defaultValue={ArchivePath}
                  id="outlined-basic"
                  label="Archive"
                  variant="outlined"
                  helperText={ArchiveError ? 'Le chemin ne doit pas contenir que des \\ seulement des /' : ''}
                  size="small"
                  error={ArchiveError}
                  onChange={(e) => {
                    if(e.target.value.includes('\\')){
                    setArchiveError(true)
                    }
                    else {
                      setArchiveError(false)
                      
                    }
                    SetArchivePath(e.target.value);
                      SetArchiveLoading(true);
                      RepertoireService(ArchivePath).then((res) => {
                        console.log('calledjfjhfjhf')
                        console.log(res)
                        SetArchiveExist(res);
                        SetArchiveLoading(false);
                      });
                  
                    
                  }}
                />
              </StyledTableCell>
              <StyledTableCell>
                {ArchiveLoading ? (
                  <CircularProgress size={30} />
                ) : (
                  <div>
                    {ArchiveExist ? (
                      <CheckCircleIcon
                        color="inherit"
                        style={{ color: "green" }}
                      />
                    ) : (
                      <ErrorIcon color="inherit" style={{ color: "red" }} />
                    )}
                  </div>
                )}
              </StyledTableCell>
            </StyledTableRow>
          </TableBody>
        </Table>
        </Paper>
        <Toolbar />
        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
          }}
        >
          
          <Button
            color="primary"
            variant="contained"
            size="medium"
            onClick={() => {
              if (InputExist && OutputExist && ArchiveExist) {
                DirectoryUpdateSerivce(InputPath, OutputPath, ArchivePath).then(
                  (res) => {
                    console.log(res);
                    if(res.status===200){
                      setOpen(true);
                    }
                  }
                );
              }
              
            }}
          >
            Sauvegarder
          </Button>
        </div>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
         <MatricesUpdate />
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
         <DatabaseUpdate id={props.dbConfigId} url={props.url} port={props.port} sid={props.sid} user={props.username} pswd={props.password} />
        </TabPanel>
      </SwipeableViews>
        
      </Paper>
    </div>
  );
}

/*<Typography variant="h6">Theme :</Typography>
      <Typography variant="body1">First color :</Typography>
      <CirclePicker onChange={(e)=>{props.setFirstColor(e.hex);}}/>
      <Typography variant="body1">Second color :</Typography>
      <CirclePicker onChange={(e)=>{props.setSecondColor(e.hex);}}/>*/

     