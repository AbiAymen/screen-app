import {withStyles, Theme, createStyles, TableCell, TableRow} from '@material-ui/core';

export const StyledTableCell= withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.white,
      color: theme.palette.common.black,
      fontWeight : 'bold'
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

 const StyledTableRow = withStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:nth-of-type(even)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }),
)(TableRow);

export default StyledTableRow


