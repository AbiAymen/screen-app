import { EndPoint } from './ServicesConfig';



export default function DirectoryUpdateSerivce(input : string , output : string , archive : string){
    return fetch(`${EndPoint}repertoires/1`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
         repIn : input,
         repOut : output,
         repArchiv : archive
        }),
      })
        
}