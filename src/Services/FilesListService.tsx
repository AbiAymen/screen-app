import { EndPoint } from './ServicesConfig';


export default function FilesListService(){
    return fetch(
        `${EndPoint}repertoires/getFileList`,
        { method: "GET" }
      )
    }
