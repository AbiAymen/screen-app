import  {EndPoint} from './ServicesConfig';

export default function MatricesListService(){
    return fetch(`${EndPoint}matrices`, { method: 'GET',})
}

export function GetMatrixService(filename : string){
    return fetch(`${EndPoint}matrices/${filename}`, { method: 'GET',})
}

export function RemoveMatrixField(id : number , foundId : number){
   return fetch(`${EndPoint}matrices/${id}/champs/${foundId}`, { method: 'DELETE',})
}

export  function UpdateMatrix(fileId : number , matrixName: string, request: string ,modifNumber: number, champs: { "id": number, "champName": string, "action": string, "valeur": string }[]) {
   return fetch(`${EndPoint}matrices/${fileId}`, {
      method: request,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        id: 202,
        matriceName: matrixName,
        nbrChampSrc: 5,
        nbrChampDest: 7,
        nbrModif: modifNumber,
        champs: champs,
      }),
    })
  }