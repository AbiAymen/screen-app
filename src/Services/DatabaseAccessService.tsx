import { EndPoint } from './ServicesConfig';


export default function getDatabaseConfigService() {
   return fetch(`${EndPoint}params`,{
       method : "GET",
 })
}

export function testDatabaseConfigService(url : string , port : number , sid : string , username : string , password : string){
 return fetch(`${EndPoint}params/test?url=${url}&port=${port}&sid=${sid}&user=${username}&pswd=${password}`,{
     method : 'GET'
 }).then((res)=>res.json())
}

export function putDatabaseConfigService(id : number , url : string , port : number , sid : string , username : string , password : string){
  return fetch(`${EndPoint}params/${id}`,{
      method : "PUT",
      headers : {"content-type":"application/json"},
      body : JSON.stringify({
          url : url,
          port : port,
          sid : sid,
          user : username,
          pswd : password
      })
  })
} 

export function postDatabaseConfigService(url : string , port : number , sid : string , username : string , password : string){
    return fetch(`${EndPoint}params`,{
        method : "POST",
        headers : {"content-type":"application/json"},
      body : JSON.stringify({
          url : url,
          port : port,
          sid : sid,
          user : username,
          pswd : password
      })
    })
  } 