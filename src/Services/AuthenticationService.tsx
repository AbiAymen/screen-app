import { EndPoint } from './ServicesConfig';


export default function AuthenticationService (username: string , password : string) {
   return fetch(`${EndPoint}login`,{
       method : "POST",
       headers : {"content-type":"application/json"},
       body : JSON.stringify({
           username : username,
           password : password
       })

   })
}

export function tokenCall(token : string) {
    return fetch(``, {
        method : 'GET',
        headers : {'content-type' : 'application/json' , 'Autorization' : token},
        
    })
}