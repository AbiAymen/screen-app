import { EndPoint } from './ServicesConfig';



export default function SendingFilesListSerivce(filesTable : {path  :string} []){
    
    return fetch(`${EndPoint}repertoires/parser`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(filesTable),
        
      })
        
}