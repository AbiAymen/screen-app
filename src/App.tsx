import React, { useState } from 'react';
import './App.css';
import GlobalView from './Components/Dashboard/GlobalView'; 
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import grey from '@material-ui/core/colors/grey';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Login from './Components/Authentication/Login';
import { blue, cyan } from '@material-ui/core/colors';




function App() {

 
  const[FirstColor, SetFirstColor] = useState(blue[500]);
  const[SecondColor, SetSecondColor] = useState(cyan[500]);
  const[TextColor, SetTextColor] = useState(grey[900]);

const Maintheme = createMuiTheme({
  palette: {
    primary: {
      main: blue[800],
      light : grey[50]
    },
    secondary: {
      main: SecondColor,
    },
    text : {
      primary : TextColor
    }
  },
});
function ProtectedRoute(props : {component : any, isAuth : boolean , path : string}){
 const Component = props.component;
 return props.isAuth ? (<Route path={props.path}><Component /></Route> ) :( <Redirect to ="/" />)
}
 
const globalView = () => <GlobalView/>





return (
    <ThemeProvider theme={Maintheme}>
      <Router>
        <div data-testid="global_view">
          <Switch>
            <ProtectedRoute path="/dashboard" component={globalView} isAuth={true}/>
            
            <Route path="/"
            >
              <Login />
              
            </Route>
          </Switch>
        </div>
      </Router>
    </ThemeProvider>
  );
}
export default App;
